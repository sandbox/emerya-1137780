<?php

/**
 *
 * Implementation action_info()
 */
function forms_alterations_action_info() {

    return array(
        'forms_alterations_delete_alteration_action' => array(
            'description' => t('Delete Alterations'),
            'type' => 'forms_alterations_data',
            'configurable' => FALSE,
            'hooks' => array('any' => TRUE),
        ),
        'forms_alterations_disable_alteration_action' => array(
            'description' => t('Disable Alterations'),
            'type' => 'forms_alterations_data',
            'configurable' => FALSE,
            'hooks' => array('any' => TRUE),
        ),
        'forms_alterations_enable_alteration_action' => array(
            'description' => t('Enable Alterations'),
            'type' => 'forms_alterations_data',
            'configurable' => FALSE,
            'hooks' => array('any' => TRUE),
        ),
    );
}

/**
 *
 * Action pour suprimer uen alteration
 * @param $object alteration_row
 * @param $context
 */
function forms_alterations_delete_alteration_action(&$object, $context = array()) {
    db_query("Delete FROM {forms_alterations} WHERE faid = %d", $object);
}

/**
 *
 * Action pour desactiver une alteration
 * @param $object alteration_row
 * @param $context
 */
function forms_alterations_disable_alteration_action(&$object, $context = array()) {
    _enableDesaibleAlteration($object, 0);
}

/**
 * Action pour activer une alteration
 * @param $object alteration_row
 * @param $context
 */
function forms_alterations_enable_alteration_action(&$object, $context = array()) {
    _enableDesaibleAlteration($object, 1);
}

/**
 *
 * Activer ou desactiver une alteration
 * @param $state
 */
function _enableDesaibleAlteration($faid, $state) {
    db_query("Update {forms_alterations} SET enabled =  %d WHERE faid = %d", $state, $faid);
}

/**
 *
 * Implementation views_bulk_operations_object_info
 */
function formsz_alterations_views_bulk_operations_object_info() {
    return array(
        'forms_alterations_data' => array(
            'type' => 'forms_alterations_data', // doit etre le meme que celui dans forms_alterations_action_info
            'base_table' => 'forms_alterations',
            'load' => '_forms_alterations_loadAlteration',
            'title' => 'alteration',
        //'access' => array('access administration pages'),
        ),
    );
}

/**
 *
 * Return une ligne d'alteraion c'est cette ligne qui va etre utilise comme param $object dans les actions
 * @param unknown_type $faid
 */
function _forms_alterations_loadAlteration($faid) {
    return $faid;
}