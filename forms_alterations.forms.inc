<?php
/**
 *
 * Afficher les element commun des formulaires
 * @param Array $defaultData valeur par defaut des champs
 */
function _elements_edit_form($elm_type = null, $defaultData=array()){
	$formTypes 		= array('checkbox','checkboxes','date','file','password','password_confirm','radio','radios','select','textarea','textfield','weight');
	$specialTypes 	= array('button','image_button','submit','hidden','token','markup','item','value');
	
	$fieldset 		= array("fieldset");
	$fromElm 		= array("form");
	$checklist		= array("checklist");
	// les element standard d'une formulaire
	$elementsWithList = array('checkboxes','radios','select','checklist');
	
	$allTypes = array_merge($formTypes , $checklist, $specialTypes, $fromElm, $fieldset);
	
	// pour cree les key dec changement du type
	$typeOptions=array_combine($allTypes, $allTypes);
	
	$elementsProperties['element_type'] = array(
	    '#type' => 'select',
	    '#title' => t('Type'),
		'#required' => TRUE,
	    '#description' => t("Element Type."),
		'#options'=>array_merge(array($elm_type => t('Default')." [$elm_type]"),$typeOptions),
		'#default_value' => _getVal($typeOptions, _getVal($defaultData,"element_type") , $elm_type),
		'for_types'=>array("all"),
	);
	
	$elementsProperties['element_fieldset'] = array(
	    '#type' => 'textfield',
	    '#title' => t('Fieldset'),
	    '#description' => t("Fieldset : define in which fieldset the element should appearl."),
		'#default_value' => _getVal($defaultData,"element_fieldset"),
		'for_types'=>$formTypes ,
	);	
	
	$elementsProperties['element_title'] = array(
	    '#type' => 'textfield',
	    '#title' => t('Label'),
	    '#description' => t("Element Label."),
		'#default_value' => _getVal($defaultData, "element_title"),
		'for_types'=>array_merge($formTypes,array("item"),$fieldset),
	);
	
	//
	$elementsProperties['element_collapsed'] = array(
	    '#type' => 'select',
	    '#title' => t('Collapsed'),
		'#options'=>array('' => t('Default'), "1"=>'yes',"0"=>'non'),
	    '#description' => t("Element Required."),
		'#default_value' => _getVal($defaultData, "element_collapsed"),
		'for_types'=>$fieldset
	);
	
	$elementsProperties['element_collapsible'] = array(
	    '#type' => 'select',
	    '#title' => t('Collapsible'),
		'#options'=>array('' => t('Default'), "1"=>'yes',"0"=>'non'),
	    '#description' => t("Element Is Collapsible."),
		'#default_value' => _getVal($defaultData, "element_collapsible"),
		'for_types'=>$fieldset
	);
	
	$elementsProperties['element_compact'] = array(
	    '#type' => 'select',
	    '#title' => t('Compact'),
	    '#description' => t("Compact Form"),
		'#options'=>array('' => t('Default'), "1"=>'yes',"0"=>'non'),
		'#default_value' => _getVal($defaultData, "element_compact"),
		'for_types'=>array('form','textfield','textarea'),
	);
	
	$elementsProperties['element_pop_description'] = array(
	    '#type' => 'select',
	    '#title' => t('Pop description'),
	    '#description' => t("Pop description"),
		'#options'=>array('' => t('Default'), "1"=>'yes',"0"=>'non'),
		'#default_value' => _getVal($defaultData, "element_pop_description"),
		'for_types'=>$fromElm,
	);
	
	$elementsProperties['element_action'] = array(
	    '#type' => 'textfield',
	    '#title' => t('Redirection'),
	    '#description' => t("Provide a URL to redirect to. Exemple : /node/12."),
		'#default_value' => _getVal($defaultData, "element_action"),
		'for_types'=>$fromElm,
	);


	$elementsProperties['element_validate'] = array(
	    '#type' => 'textarea',
	    '#title' => t('validate'),
	    '#description' => t("A list of custom validation functions"),
		'#default_value' => _getVal($defaultData, "element_validate"),
		'for_types'=>$fromElm,
	);
	
	$elementsProperties['element_value'] = array(
	    '#type' => 'textfield',
	    '#title' => t('Value'),
	    '#description' => t("Element Value."),
		'#default_value' => _getVal($defaultData, "element_value"),
		'for_types'=>$specialTypes,
	);

	$elementsProperties['element_description'] = array(
	    '#type' => 'textfield',
	    '#title' => t('Help'),
	    '#description' => t("Element Help."),
		'#default_value' => _getVal($defaultData, "element_description"),
		'for_types'=>array_merge($formTypes,array("item")),
	);

	$elementsProperties['element_default_value'] = array(
	    '#type' => 'textarea',
	    '#title' => t('Default value'),
	    '#description' => t("Enter a default value. You can also return an array for multi-value field like <i>return array('key1', 'key2);</i> included in PHP tags."),
		'#default_value' => _getVal($defaultData, "element_default_value"),
		'for_types'=>$formTypes,
	);
	
	$elementsProperties['element_multiple'] = array(
	    '#type' => 'select',
	    '#title' => t('Multiple'),
		'#options'=>array('' => t('Default'), "1"=>'yes',"0"=>'non'),
	    '#description' => t("Element has multiple choice."),
		'#default_value' => _getVal($defaultData, "element_multiple"),
		'for_types'=>array("select",'checklist'),
	);

	$elementsProperties['element_options'] = array(
	    '#type' => 'textarea',
	    '#title' => t('Options'),
	    '#description' => t("Element options.(une option par line)"),
		'#default_value' => _getVal($defaultData, "element_options"),
		'for_types'=>$elementsWithList,
	);
	
	$elementsProperties['element_weight'] =	array(
		'#type' => 'select',
		'#title' => t('Weight'),		
		'#options' => _getWeightValues(10),
		'#default_value' => $settings[$key.'_weight_alter'],
		'#description' =>  t("Element Weight."),
		'#default_value' => _getVal($defaultData, "element_weight"),
		'for_types'=>$allTypes,
	);

	$elementsProperties['element_prefix'] = array(
	    '#type' => 'textfield',
	    '#title' => t('Prefix'),
	    '#description' => t("Element Prefix."),
		'#default_value' => _getVal($defaultData, "element_prefix"),
		'for_types'=>$allTypes,
	);

	$elementsProperties['element_suffix'] = array(
	    '#type' => 'textfield',
	    '#title' => t('Suffix'),
	    '#description' => t("Element Suffix."),
		'#default_value' => _getVal($defaultData, "element_suffix"),
		'for_types'=>$allTypes,
	);

	$elementsProperties['element_required'] = array(
	    '#type' => 'select',
	    '#title' => t('Required'),
		'#options'=>array('' => t('Default'), "1"=>'yes',"0"=>'non'),
	    '#description' => t("Element Required."),
		'#default_value' => _getVal($defaultData, "element_required"),
		'for_types'=>$formTypes
	);
	
	$elementsProperties['element_autocomplete_path'] = array(
	    '#type' => 'textfield',
	    '#title' => t('Autocomplete path'),
		'#default_value' => _getVal($defaultData, "element_autocomplete_path"),
	    '#description' => t("Use a normal path or autocomplete/%viewName% for views"),
		'for_types' => array("textfield"),
	);
	
	$elementsProperties['element_other_settings'] = array(
	    '#type' => 'textarea',
	    '#title' => t('Extra settings'),
		'#default_value' => _getVal($defaultData, "element_other_settings"),
	    '#description' => t("Free sittings for element<br> format #key | value"),
		'for_types' => array("all"),
	);

	$elementsProperties['attributes_target'] = array(
	    '#type' => 'textfield',
	    '#title' => t('Form target'),
		'#default_value' => _getVal($defaultData, "attributes_target"),
	    '#description' => t("Form Target"),
		'for_types' => $fromElm,
	);
	
	$elementsProperties['extrasettings_lightbox_events'] = array(
	    '#type' => 'textarea',
	    '#title' => t('Close lightbox'),
		'#default_value' => _getVal($defaultData, "extrasettings_lightbox_events"),
	    '#description' => t("Close light box on this events one per line<br> Ex:<br> $('#node_form').submit <br> $('.form-submit').click"),
		'for_types' => $fromElm,
	);
	
	$elementsProperties['element_new'] = array(
	    '#type' => 'hidden',
	    '#title' => t('Is new element'),
		'#default_value' => _getVal($defaultData, "element_new",0),
		'for_types' => array("all"),
	);
	
	$form = _form_alteration_fieldset($defaultData);
	$form['forms_alterations']['element'] = array(
	    '#type' => 'fieldset',
	    '#title' => t('Element Infos'),
	);
	
	foreach($elementsProperties as $key=>$property){
		$typeClass = "for_elements for_".implode(" for_", $property['for_types']);
		
		if(!in_array("all",$property['for_types']) && !in_array($elm_type, $property['for_types'])){
			$typeClass .= " notfor_thistype";
		}
		
		$property['#prefix']='<div class="'.$typeClass.'">';
		$property['#suffix']='</div>';
		unset($property['for_types']);
		
		$form['forms_alterations']['element'][$key] = $property;
	}

	
	// for new types
	$form['forms_alterations']['alteration']['default_type'] = array(
	    '#type' => 'hidden',
	    '#title' => t('Default type'),
	    '#description' => t("Default type."),
		'#default_value' => $elm_type,
	);
	
	
	
	// fix form elements
	//$form['forms_alterations']['alteration']['alteration_element']['#attributes']['readonly'] = 'readonly';
	//$form['forms_alterations']['alteration']['alteration_form_id']['#attributes']['readonly'] = 'readonly';

	$form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
	
	if(_getVal($defaultData, "alteration_faid",false)){
		$form['delete'] = array('#type' => 'submit', '#value' => t('Delete'));
	}
	
	return $form;
}

/**
 *
 * ajoute dans le formulaire le fieldset qui contient la partie commun de alteration
 * @param $defaultData
 */
function _form_alteration_fieldset($defaultData){
	$form['forms_alterations']['alteration'] = array(
	    '#type' => 'fieldset',
	    '#title' => t('Alteration info'),
	);	
	
	$form['forms_alterations']['alteration']['alteration_title'] = array(
	    '#type' => 'textfield',
	    '#title' => t('Title'),
		'#required' => TRUE,
	    '#description' => t("Internal title of the alteration."),
		'#default_value' => _getVal($defaultData, "alteration_title"),
	);
	
	$form['forms_alterations']['alteration']['alteration_faid'] = array(
	    '#type' => 'hidden',
	    '#title' => t('Alteration id'),
	    '#description' => t("Alteration id vislibe temp."),
		'#default_value' => _getVal($defaultData, "alteration_faid"),
	);

	$form['forms_alterations']['alteration']['alteration_form_id'] = array(
	    '#type' => 'textfield',
	    '#title' => t('Form'),
		'#required' => TRUE,
	    '#description' => t("Alteration Form."),
		'#default_value' => _getVal($defaultData, "alteration_form_id"),
	);

	$form['forms_alterations']['alteration']['alteration_element'] = array(
	    '#type' => 'textfield',
	    '#title' => t('Element'),
		'#required' => TRUE,
	    '#description' => t("ID of alterated element. 0 for the form itself."),
		'#default_value' => _getVal($defaultData, "alteration_element"),
	);

	$form['forms_alterations']['alteration']['alteration_enabled'] = array(
		'#type' => 'checkbox',
	    '#title' => t('Alteration is active'),
			'#options'=>array(1,0),
	    '#description' => t("Uncheck this box to desactive the alteration."),
		'#default_value' => _getVal($defaultData, 'alteration_enabled', 1),
	);

	$form['forms_alterations']['alteration']['alteration_conditions'] = array(
	    '#type' => 'textarea',
	    '#title' => t('Condition'),
	    '#description' => t("PHP condition of the validation. Include <?php and ?>. Let empty to have no conditions"),
		'#default_value' => _getVal($defaultData, "alteration_conditions"),
	);

	return $form;
}

function _getVal($array,$key,$default=""){
	if(isset($array[$key])){
		return  $array[$key];
	}
	return $default;
}

function _getWeightValues($max){
	$weight = array();
	for($i=-$max; $i <= $max; $i++){
		$weight[$i] = $i;
		if($i == 0){
			$weight[''] = t('Default');
		}
	}
	return $weight;
}