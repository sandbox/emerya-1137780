<?php
/**
 *
 * Surcharger les settings d'un element depuis une alteration valide
 * @param unknown_type $element
 * @param unknown_type $form_id
 * @param unknown_type $element_id
 * @param unknown_type $formAlterations
 */
function _get_element_options($element,$form_id,$element_id,&$formAlterations = array()){
	$debug = variable_get('forms_alterations_debug', 0);
	if(_is_valid_elements($element)){
		if( $element['#type']=="form" || $element['#type']=="fieldset" || empty($element['#type'])){
			foreach(element_children($element) as $key){
				$fieldset_key = $formAlterations[$key][0]->settings['fieldset'];
				if($fieldset_key and !$element[$fieldset_key]) {
					$element[$fieldset_key]['#type'] = 'fieldset'; //Make sure there is a fieldset
					if ($fieldset_key) { //Apply the fieldset settings if it hase been created with form alteration because creation occurs after alterations
						$element[$fieldset_key]['#title'] = $formAlterations[$fieldset_key][0]->settings['title'];
						$element[$fieldset_key]['#collapsed'] = $formAlterations[$fieldset_key][0]->settings['collapsed'];
						$element[$fieldset_key]['#collapsible'] = $formAlterations[$fieldset_key][0]->settings['collapsible'];
						$element[$fieldset_key]['#description'] = $formAlterations[$fieldset_key][0]->settings['description'];
						unset($formAlterations[$fieldset_key]);
					}				
					$element[$fieldset_key][$key] = $element[$key];
					unset($element[$key]);
				}			
			
				$element[$key] = _get_element_options($element[$key],$form_id,$key,$formAlterations);
			}
		}
		$access = user_access('add Alteration');
		if(!empty($element['#type'])) {
			if(isset($formAlterations[$element_id])){
				//get element alteration
				foreach($formAlterations[$element_id] as $alteration){
					//dsm($alteration);
					$condition = _conditionCorrect($alteration->conditions);
					if($alteration->enabled && $condition){
						$element = _apply_alteration($element,$alteration);
					} else {
						if($access && $debug) {
							if (!$access)
							$enabled_message = t('alteration is disabled');
							if (!$alteration->enabled){
								if (!$access)
								$enabled_message .= t(' and ');
								$enabled_message .= t('condition is evaluated to false');
							}
							drupal_set_message(t('<b>Alteration:</b> !title is not active (!message) <a href="!link">Modify</a>', array('!message' => $enabled_message, '!title' => $alteration->title, '!link' => FORM_ALTERATION_ADMIN_EDIT.'?alteration_id='.$alteration->faid.'&elm_type='.$element['#type'].'&destination='.request_uri() )),'status');
						}
					}
				}
				unset($formAlterations[$element_id]);
			}
		}
	}
	return $element;
}
 
/**
 * Ajouter un nouveau element sur le formulaire
 * @param unknown_type $form
 * @param unknown_type $form_id
 * @param unknown_type $elmAlterations
 * @return Ambigous <boolean, unknown_type>
 */
function _add_new_Element(&$form,$form_id,$elmAlterations){
	foreach($elmAlterations as $alteration){
		$condition = _conditionCorrect($alteration->conditions);
		if($alteration->enabled && $condition && $alteration->settings['type']<>'fielset'){
			$element = array();
			$element = _apply_alteration($element,$alteration);
		} else {
			if($access && $debug) {
				if (!$access)
					$enabled_message = t('alteration is disabled');
				if (!$alteration->enabled){
					if (!$access)
					$enabled_message .= t(' and ');
					$enabled_message .= t('condition is evaluated to false');
				}
				drupal_set_message(t('<b>Alteration:</b> !title is not active (!message) <a href="!link">Modify</a>', array('!message' => $enabled_message, '!title' => $alteration->title, '!link' => FORM_ALTERATION_ADMIN_EDIT.'?alteration_id='.$alteration->faid.'&elm_type='.$element['#type'].'&destination='.request_uri() )),'status');
			}
		}
	}
	
	$form[$alteration->element] = $element;
	
	return $form;
}

/**
 * Appliquer une alteration sur un element
 * @param unknown_type $element
 * @param unknown_type $alteration
 * @return boolean
 */
function _apply_alteration($element,$alteration){
	//echo $alteration->faid," ", $alteration->element,' ', $alteration->settings['weight'],'<br>';
	global $loadChecklist,$loadBt;
	
	if($access && $debug)
		drupal_set_message(t('<b>Alteration:</b> !title <a href="!link">Modify</a>', array('!title' => $alteration->title, '!link' => FORM_ALTERATION_ADMIN_EDIT.'?alteration_id='.$alteration->faid.'&elm_type='.$element['#type'].'&destination='.request_uri() )),'status');
	//get element settings
	foreach($alteration->settings as $settingKey => $setting){
		if(($settingKey != "other_settings" && $settingKey != "extrasettings") && ( isset($element['#type']) || $alteration->settings['new']==1)){
			if($settingKey != "default_value"  || $element['#default_value'] == ""){
				$element['#'.$settingKey] = str_replace('<none>', '', eval_setting($setting,$settingKey,$alteration->element,$alteration->faid,$element['#type']));
			}
		}
	}
	// appliquer les changements le champs other setting
	if(isset($alteration->settings['other_settings'])){
		$other_settings = _stringToArray($alteration->settings['other_settings']);
		foreach($other_settings as $key=>$other_setting){
			$element[$key] = $other_setting;
		}
	}
	
	if(isset($alteration->settings['extrasettings'])){
		foreach($alteration->settings['extrasettings'] as $key=>$extrasetting){
			if($key == "lightbox_events"){
				$lightbox_events = _stringToArray($extrasetting);
				$jsScripts = array();
				foreach($lightbox_events as $lightbox_event){
					$jsScripts[] = "$lightbox_event(function(event){
						window.close();
						window.parent.Lightbox.end('forceClose');
					})"; 
				}
				$jsScript = implode("\n", $jsScripts);
				$jsScript = "
					$('document').ready(function(){
						$jsScript
					});
				";
				drupal_add_js($jsScript, 'inline');
			}
		}
	}

	// tester si type est checklist si loadChecklist n'est pas deja true
	if($loadChecklist == false && $alteration->settings['type'] == "select" 
		&& isset($alteration->settings['attributes']['class']) 
		&& preg_match("/.*elm_type_checklist.*/",$alteration->settings['attributes']['class']))	{
		$loadChecklist = true;
	}
	
	// test pour loadé les js de pop-up description
	if($loadBt == false && $alteration->settings['type'] == "form" 
		&& isset($alteration->settings['attributes']['class']) 
		&& preg_match("/.*pop_description.*/",$alteration->settings['attributes']['class']))	{
		$loadBt = true;
	}

	if ( $element['#type'] == 'hidden' )
		$element['#required'] = FALSE;
		
	
	return $element;
}

/**
 * 
 * Evlauer une setting
 * @param $setting
 * @param $settingKey
 * @param $element_id
 * @param $alteration_id
 * @param $elmType
 */
function eval_setting($setting,$settingKey,$element_id=null,$alteration_id=null,$elmType = null){
	$return = $setting;
	if(is_php_code($setting)){
		ob_start();
		$return = eval('?>' . $setting);
		if ('' !== $error = ob_get_clean()) {
    		drupal_set_message(t('<b>Error When Calling Eval For:</b><br>
    		Element : !element<br />
    		Setting : !setting<br />
    		Value : !value<br />
    		<a href="!link">Modify Alteration</a>', array(
    			'!element' => $element_id, 
    			'!setting' => $settingKey, 
    			'!value' => htmlspecialchars($setting), 
    			'!link' => SITE_BASE_URL.'/'.FORM_ALTERATION_ADMIN_EDIT.'?alteration_id='.$alteration_id.'&elm_type='.$elmType.'&destination='.request_uri() )),'error');
		}
		
	}
	if($settingKey == "options" && !is_array($return)){
		$return = _stringToArray($return);
	}
	return $return;
}

/**
 * Tester si une string est bien un code php valid
 * @param unknown_type $code
 * @return boolean
 */
function is_php_code($code){
	return is_string($code) && preg_match("/.*<\?php.*/", $code);
}

/**
 * transfomer un string en array (key | value \n)
 * @param unknown_type $string
 * @return Ambigous <multitype:, string>
 */
function _stringToArray($string){
	$options = array();
	$optionArray=explode("\n", $string);
	foreach($optionArray as $_option){
		if(trim($_option) != ""){
			$optionParts =  explode("|",$_option);
			$val = $key = $optionParts[0];
			if(isset($optionParts[1])){
				$val = $optionParts[1];
			}
			$options[trim($key)] = trim($val);
		}
	}
	return $options;
}


/**
 *
 * Ajouter le menu pour l'ajout des alteration
 * @param unknown_type $element
 * @param unknown_type $form_id
 * @param unknown_type $element_id
 * @param unknown_type $formAlterations
 */
function _get_element_menu($element,$form_id,$element_id,$formAlterations = array()){
	if(_is_valid_elements($element)){
		if( $element['#type']=="form" || $element['#type']=="fieldset" || empty($element['#type'])){
			foreach(element_children($element) as $key){
				$element[$key] = _get_element_menu($element[$key],$form_id,$key,$formAlterations);
			}
		}

		if(!empty($element['#type'])) {
			// get element menu
			$menuItem = _get_element_menu_itemes($form_id,$element_id,$formAlterations,$element['#type']);
			if(in_array($element['#type'], array('form',"hidden","token"))){
				global $formMenu;
				$formMenu .= '<span>['.$element['#type'].'] '.$element_id.$menuItem.'</span>';
				if($element['#type'] == "form"){
					//_add_class_to_attributes($element, "form_with_menus","#");
				}

			} else {
				$element['#prefix'] .= $menuItem;
			}
		}
	}
	return $element;
}

/**
 *
 * ajouter le menu apres un element
 * @param unknown_type $form_id
 * @param unknown_type $element_id
 * @param unknown_type $formAlterations
 * @param unknown_type $elm_type
 */
function _get_element_menu_itemes($form_id,$element_id,$formAlterations = array(),$elm_type){
	$url_parms = "form_id=$form_id";
	$ulId="frm_$form_id";
	if($element_id != "0"){
		$url_parms .= "&element_id=$element_id";
		$ulId = "elm_$element_id";
	}

	$url_parms .= "&elm_type=$elm_type" . '&destination=' . request_uri();
	
	$rel = '';
	$lightbox = variable_get('forms_alterations_lightbox2', 0);
	if ($lightbox and module_exists('lightbox2')) {
		$rel = ' rel="' . $lightbox .'[|width:790px;height:680px;][' . t('Alter !type !id from the form !form-id', array( '!type' => $elm_type, '!id' => $element_id, '!form-id' => $form_id)) . ']" ';
		$lightbox_url = '/lightbox2';
	}
	
	$menuStr = '<div class="forms_alterations_menu hide" id="'.$ulId.'">&nbsp;<ul>'
	.'<li><a ' . $rel .  'href="'.SITE_BASE_URL.'/'.FORM_ALTERATION_ADMIN_EDIT.$lightbox_url.'?'.$url_parms.'">'.t("Add new Alteration").'</a></li>';

	if(isset($formAlterations[$element_id])){
		foreach($formAlterations[$element_id] as $alteration){
			$menuStr.='<li><a ' . $rel .  'href="'.SITE_BASE_URL.'/'.FORM_ALTERATION_ADMIN_EDIT.$lightbox_url.'?alteration_id='.$alteration->faid.'&elm_type='.$elm_type.'&destination='.request_uri().'">'.t("Edit Alteration").": ".$alteration->title.'</a></li>';
		}
	}
	$menuStr .='</ul></div>';

	return $menuStr;
}

function _is_valid_elements($elm){
	//return $elm['#type'] !="hidden";
	return true;
}

/**
 *
 * tester si une condition et bien satisfaite
 * @param unknown_type $condition
 */
function _conditionCorrect($condition){
	if(trim($condition)==""){
		return true;
	}
	return @eval("$condition;");
}


/**
 *
 * get all alteration of this form
 * @param String $from_id
 */
function _get_form_alterations($from_id){
	$query = "SELECT * From {forms_alterations} WHERE fid = '%s'";
	$query_result =  db_query($query, $from_id);
	$formAlterations = array();
	while ($alteration = db_fetch_object($query_result)) {
		$alteration->settings = unserialize($alteration->settings);
		$formAlterations[$alteration->element][] = $alteration;
	}
	return $formAlterations;
}