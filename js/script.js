$(document).ready(function(){
	if(loadJs["dropdownchecklist"]) {
		$(".elm_type_checklist").dropdownchecklist();
	}
	compactForm();
	if(loadJs["bt"]) {
		popDescription();
	}
	
});

function popDescription() {
	$('.pop_description .form-item').each(function(){
		if($(this).find('.description').length > 0){
			content = $(this).find('.description').html();
			$(this).bt(content,{
				positions: [ 'top', 'left']
			});
		}
	});
}

function compactForm(){
	$('form.compact_form input[type="text"], form.compact_form texterea, .compact_form  ').each(function() {
		$(this).parents(".form-item").addClass('compact_div').addClass('hideLabel');
		if($(this).val() == ""){
			$(this).parents(".form-item").removeClass('hideLabel');
		}
		
		$(this).focus(function(){
			$(this).parents(".form-item").addClass('hideLabel');
		});
		$(this).blur(function() {
			if($(this).val() == ""){
				$(this).parents(".form-item").removeClass('hideLabel');
			}
		})
	})
}