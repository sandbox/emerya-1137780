$(document).ready(function(){
	fixMenu()
	hideNotElementFields();
});

function hideNotElementFields(){
	if($('#edit-element-type').length > 0){
		_showfields(_getType($('#edit-element-type')));
		$('#edit-element-type').click(function(){
			_showfields(_getType(this));
		})
	}
}

function _getType(elm){
	_elmenetType = $(elm).val();
	if(_elmenetType==""){
		_elmenetType = $('#edit-default-type').val()
	}
	return _elmenetType;
}


function _showfields(_elmenetType){
	_elmenetTypeCLass = ".for_"+_elmenetType;
	// hide all inputes
	$(".for_elements").hide();
	$(".for_elements").find("input,select,textarea").attr('disabled',"disabled");
	
	// show current type valide inputes
	$(_elmenetTypeCLass).show();
	$(_elmenetTypeCLass).find("input,select,textarea").attr('disabled',false);
}

function fixMenu(){
	if($('#form_edition_and_hiden').length > 0){
		$('#form_edition_and_hiden').hover(function(){
			$(this).addClass('hover')
		},function(){
			$(this).removeClass('hover')
		})
		
		$("#form_edition_and_hiden .form_sub_menu span").hover(function(){
			$(this).find('.forms_alterations_menu').addClass('hover');
		},function(){
			$(this).find('.forms_alterations_menu').removeClass('hover');
		})
	}
	
	$("input[type!='hidden'],select,textarea").each(function(){
		elmName= $(this).attr("name");
		elmMenuId = "#elm_"+elmName;
		if($(elmMenuId).length>0){
			$(this).before($(elmMenuId).clone());
			$(elmMenuId).remove();
		}
	})
}